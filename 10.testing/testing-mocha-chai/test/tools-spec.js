const {expect} = require('chai');
const tools = require('../lib/tools');
const nock = require('nock');

describe('Tools', function() {
  describe('printName()', function() {
    it('should print the last name first', function() {
      const results = tools.printName({first: 'Tien', last: 'Nguyen'});
      expect(results).to.equal('Nguyen, Tien');
    });
  });

  describe('loadWiki()', function() {
    before(function () {
      nock('https://en.wikipedia.org')
        .get('/wiki/Steve_Jobs')
        .reply(200, 'Mock Steve Jobs Page');
    });

    it('should load Steve Jobs wikipedia page', function(done) {
      tools.loadWiki({first: 'Steve', last: 'Jobs'}, function(html) {
        expect(html).to.equal('Mock Steve Jobs Page');
        done();
      });
    });
  });
});