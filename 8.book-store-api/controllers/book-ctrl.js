const Book = require('../models/Book');

exports.getAllBooks = (req, res, next) => {
  Book.find({}, (err, books) => {
    if (err) return next(err);
    res.status(200).json({books});
  });
};

exports.addBook = (req, res, next) => {
  const {title, author, image, release_date, pages} = req.body;
  Book.findOne({title, pages, release_date}, (err, book) => {
    if (err) return next(err);
    if (book) return res.status(400).json({message: 'Book has already existed'});
    const newBook = new Book(req.body);
    newBook.save(err => {
      if (err) return next(err);
      res.json({
        message: 'Successfully create new book',
        book: newBook
      });
    });
  });
};

exports.getBookById = (req, res, next) => {
  Book.findOne({_id: req.params._id}, (err, book) => {
    if (err) return next(err);
    if (!book) return res.status(404).json({message: 'No book found'});
    res.status(200).json({book});
  })
};

exports.updateBookById = (req, res, next) => {
  Book.findOne({_id: req.params._id}, (err, book) => {
    if (err) return next(err);
    if (!book) return res.status(404).json({message: 'No book found to update'});
    const {title, author, image, release_date, pages} = req.body;
    const updatedTitle = title || book.title;
    const updatedAuthor = author || book.author;
    const updatedImage = image || book.image;
    const updatedReleaseDate = release_date || book.release_date;
    const updatedPages = pages || book.pages;
    Book.findOneAndUpdate(
      {
        _id: req.params._id
      },
      {
        $set: {
          title: updatedTitle,
          author: updatedAuthor,
          image: updatedImage,
          release_date: updatedReleaseDate,
          pages: updatedPages
        }
      },
      {
        new: true,
        upsert: true
      },
      (err, book) => {
        if (err) return next(err);
        res.json({
          message: 'Successfully update book',
          book
        });
      }
    );
  });
};

exports.deleteBookById = (req, res, next) => {
  Book.findOneAndRemove({_id: req.params._id}, (err) => {
    if (err) return next(err);
    res.status(200).json({message: 'Successfully delete book'});
  })
};