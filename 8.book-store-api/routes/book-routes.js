const Router = require('express').Router();
const bookCtrl = require('../controllers/book-ctrl');

Router.get('/books', bookCtrl.getAllBooks);
Router.post('/books', bookCtrl.addBook);
Router.get('/books/:_id', bookCtrl.getBookById);
Router.put('/books/:_id', bookCtrl.updateBookById);
Router.delete('/books/:_id', bookCtrl.deleteBookById);

module.exports = Router;