const Router = require('express').Router();
const bookRoutes = require('./book-routes');

Router.get('/', (req, res) => {
  res.status(200).json({
    message: "Welcome to Book Store API",
    documentation: {
      "getAllBooks": "GET /api/books",
      "addABook": "POST /api/books",
      "getBookById": "GET /api/books/:id",
      "updateBookById": "PUT /api/books/:id",
      "deleteBookById": "DELETE /api/books/:id"
    }
  });
});

Router.use('/api', bookRoutes);

Router.use((req, res) => {
  res.status(404).json({message: 'Not Found'});
});

module.exports = Router;