const express = require('express');
const app = express();
const compression = require('compression');
const helmet = require('helmet');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
const mongoose = require('mongoose');
const routes = require('./routes');

//Logger
app.use(logger('dev'));

//Security
app.use(helmet());

//Gzip
app.use(compression());

//CORS
app.use(cors());

//MongoDB
const configDB = require('./config/database');
mongoose.Promise = global.Promise;
mongoose.connect(configDB.url);
mongoose.connection
  .once('open', () => console.log('Connected to MongoLab instance'))
  .on('error', err => console.log('Error connecting to MongoLab:', err));

//Body Parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

//Routes
app.use(routes);

//Server
app.listen(8080, () => {
  console.log('Server listening on port 8080');
});

module.exports = app;

