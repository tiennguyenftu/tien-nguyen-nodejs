const mongoose = require('mongoose');
const {Schema} = mongoose;

const bookSchema = new Schema({
  title: {type: String},
  image: {type: String},
  author: {type: String},
  release_date: {type: Date},
  pages: {type: Number}
});

module.exports = mongoose.model('Book', bookSchema);