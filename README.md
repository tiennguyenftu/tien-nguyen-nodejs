Require: Node.js version `>= 6.x`  
This project includes many sub-projects to demonstrate knowledge about Node.js.  
### `bmi-calculator`:
- Global Objects in Node.js: `process` (`process.stdin`, `process.stdout`, `process.exit`)
- How to run: `node app.js`

### `readline`:
- `readline` node module
- How to run: `node app.js`

### `event-emitter`:
- EventEmitter
- `module.exports`
- `require`
- How to run: `node app.js`

### `child-process`
- `exec`
- `spawn`
- `os` node module
- `setTimeout`, `setInterval` - global timing functions
- How to run: `node exec.js` or `node spawn.js`

### `file-system`
- `fs.readdir`, `fs.readdirSync`: Read a folder
- `fs.readFile`, `fs.readFileSync`: Read a file
- `fs.writeFile`, `fs.writeFileSync`: Write content to a file
- `fs.appendFile`, `fs.appendFileSync`: Append content to a file
- `fs.mkdir`, `fs.mkdirSync`: Make a directory
- `fs.rename`, `fs.renameSync`: Rename a file or directory
- `fs.unlink`, `fs.unlinkSync`: Remove a file
- `fs.rmdir`, `fs.rmdirSync`: Remove a empty directory
- Use `rmdir` node module to remove a not empty directory
- `fs.createReadStream`, `fs.createWriteStream`: Create read or write stream
- `fs.exists`, `fs.existsSync`: Check if a file or directory exists
- `fs.stats`, `fs.statsSync`: Get stats of a file or directory
- How to run: `node <file>`

### `http`
- Make a request
- Create a web server
- Serve static files
- Serve json data
- Handle POST request
- How to run: `node <file>`

### `httpster`
- Use node modules from npm
- Serve static files with httpster
- How to run: 
``` 
npm install httpster
./node_modules/httpster/bin/httpster -p 8080 -d ./public/
```

### `book-store`
Create a web api for books using Express.js  
- `express.Router()`: create api routes  
- `body-parser`: parse form data and json  
- `morgan`: log http requests  
- `helmet`: security  
- `mongoose`: mongodb ORM  
- `compression`: gzip  
- `cors`: cross origin resources sharing  
How to run:
``` 
npm install
npm run dev (or npm start)
```

### `web-sockets`
- `ws`:
Using `ws` node module on the server and `WebSocket` native browser module on the clients  
How to run:
```
npm install
npm run start-mac (for MacOS)
npm run start-ubuntu (for Ubuntu)
```
- `socket-io`:
Using `socket.io` node module  
How to run: 
``` 
npm install
npm start
```

### `testing`
#### `testing-mocha-chai`  
- Testing with `mocha` and `chai`  
- Asynchronous mocha testing  
- Mocking a server with `nock`  
- Inject dependencies with `rewire` and `sinon` spies and stubs  
- Code coverage with `istanbul`  
- Testing http endpoints with `supertest`  
How to run
``` 
sudo npm install -g mocha istanbul
npm install
npm test
```

#### `testing-http`  
- Testing http endpoints with `supertest`  
- Checking server response with `cheerio`  
How to run
``` 
npm install
npm test
```
### `automation-deployment`
- Hint code with `grunt`
- Convert LESS to CSS with `grunt`
- Build client scripts with `browserify`
- Watch file changes with `grunt-watch`
- Automation with `npm`
- Debug with `npm`
How to run
``` 
npm i -g grunt-cli
npm install
npm start
```