const Employee = require('./Employee');

const dev = new Employee('Tien', 'developer');

dev.on('greeting', function (favoriteQuote) {
  console.log(`Hello. My name is ${this.name}`);
  console.log(`I am a ${this.job}`);
  console.log(`My favorite quote is "${favoriteQuote}"`)
});

dev.emit('greeting', 'Stay hungry. Stay foolish');