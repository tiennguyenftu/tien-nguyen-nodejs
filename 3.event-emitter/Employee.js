const {EventEmitter} = require('events');
const util = require('util');

function Employee(name, job) {
  this.name = name;
  this.job = job;
}

util.inherits(Employee, EventEmitter);

module.exports = Employee;