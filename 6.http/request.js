const https = require('https');
const fs = require('fs');
const path = require('path');

const options = {
  hostname: 'www.google.com.vn',
  port: 443,
  path: '/',
  method: 'GET'
};

const req = https.request(options, res => {
  let resBody = '';
  console.log('Response from server started');
  console.log(`Server status: ${res.statusCode}`);
  console.log('Response Headers:', res.headers);

  res.setEncoding('UTF-8');

  res.once('data', chunk => {
    console.log(chunk);
  });

  res.on('data', chunk => {
    console.log(`---chunk--- ${chunk.length}`);
    resBody += chunk;
  });

  res.on('end', () => {
    fs.writeFile(path.join(__dirname, 'public', 'google.html'), resBody, err => {
      if (err) console.log(err);
      console.log('File downloaded');
    });
  });
});

req.on('error', err => {
  console.log('Problem with request', err);
});

req.end();