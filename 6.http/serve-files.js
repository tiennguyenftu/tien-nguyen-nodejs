const http = require('http');
const fs = require('fs');
const path = require('path');

http.createServer((req, res) => {
  console.log(`${req.method} request for ${req.url}`);
  if (req.url === '/') {
    fs.readFile(path.join(__dirname, 'public', 'index.html'), (err, html) => {
      res.writeHead(200, {'Content-Type': 'text/html'});
      res.end(html);
    });
  } else if (req.url.match(/.css$/)) {
    const cssPath = path.join(__dirname, 'public', req.url);
    const fileStream = fs.createReadStream(cssPath, 'UTF-8');

    res.writeHead(200, {'Content-Type': 'text/css'});

    fileStream.pipe(res);
  } else if (req.url.match(/.jpg$/)) {
    const imgPath = path.join(__dirname, 'public', req.url);
    const fileStream = fs.createReadStream(imgPath);

    res.writeHead(200, {'Content-Type': 'image/jpeg'});

    fileStream.pipe(res);
  } else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.end('404 File Not Found');
  }
}).listen(8080);
console.log('Server listening on port 8080');