const http = require('http');
const books = require('./data/books.json');

http.createServer((req, res) => {
  if (req.url === '/') {
    res.writeHead(200, {'Content-Type': 'text/json'});
    res.end(JSON.stringify(books));
  } else if (req.url === '/long-books') {
    const longBooks = books.filter(book => book.pages >= 300);
    res.writeHead(200, {'Content-Type': 'text/json'});
    res.end(JSON.stringify(longBooks));
  } else if (req.url === '/nodejs-books') {
    const nodeJSBooks = books.filter(books => books.title.toLowerCase().includes('node'));
    res.writeHead(200, {'Content-Type': 'text/json'});
    res.end(JSON.stringify(nodeJSBooks));
  } else {
    res.writeHead(404, {'Content-Type': 'text/plain'});
    res.end('404 Not Found');
  }
}).listen(8080);
console.log('Server listening on port 8080');