const fs = require('fs');

//Read all files in a folder
//Synchronous
// const files = fs.readdirSync('./files');
// console.log(files);

//Asynchronous
fs.readdir('./files', (err, files) => {
  if (err) return console.log('Err when read files folder', err);
  console.log(files);
});