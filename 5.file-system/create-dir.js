const fs = require('fs');

//Synchronous
// fs.mkdirSync('./folder');
// console.log('Successfully create new folder');

//Asynchronous
fs.mkdir('./folder', (err) => {
  if (err) console.log('Error', err);
  console.log('Successfully create new folder');
});