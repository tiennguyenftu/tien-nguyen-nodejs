const fs = require('fs');
const path = require('path');

const bigFilePath = path.join(__dirname, 'big-files', 'big.txt');
const newBigFilePath = path.join(__dirname, 'big-files', 'new-big.txt');

const readStream = fs.createReadStream(bigFilePath, 'UTF-8');
const writeStream = fs.createWriteStream(newBigFilePath, 'UTF-8');
let data = '';

readStream.once('data', () => {
  console.log('Start reading big.txt file');
});

readStream.on('data', chunk => {
  console.log(`chunk: ${chunk.length}`);
  data += chunk;
  writeStream.write(chunk);
});

readStream.on('end', () => {
  console.log('Finish reading big.txt file', data.length);
  console.log('Successfully write new-big.txt file');
});
