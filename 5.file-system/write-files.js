const fs = require('fs');
const path = require('path');

const filePath = path.join(__dirname, 'files', 'ipsum.txt');
const newFilePath = path.join(__dirname, 'files', 'new-ipsum.txt');


//Synchronous
// const content = fs.readFileSync(filePath, 'UTF-8');
// //Write file
// fs.writeFileSync(newFilePath, content, 'UTF-8');
// //Append file
// fs.appendFileSync(newFilePath, content, 'UTF-8');
// console.log('Successfully write new-ipsum.txt file');

//Asynchronous
fs.readFile(filePath, 'UTF-8', (err, content) => {
  if (err) return console.log('Error when read ipsum.txt file', err);
  //Write file
  fs.writeFile(newFilePath, content, 'UTF-8', err => {
    if (err) return console.log('Error when write new-ipsum.txt file', err);
    //Append file
    fs.appendFile(newFilePath, content, 'UTF-8', err => {
      if (err) return console.log('Error when append new-ipsum.txt file', err);
      console.log('Successfully write new-ipsum.txt file');
    });
  });
});