const fs = require('fs');

//Synchronous
// //Rename
// fs.renameSync('./folder', './new-folder');
// console.log('Successfully rename folder to new-folder');
// //Remove
// fs.rmdirSync('./new-folder');
// console.log('Successfully remove new-folder');

//Async
//Rename
fs.rename('./folder', './new-folder', err => {
  if (err) return console.log('Err when rename folder', err);
  console.log('Successfully rename folder to new-folder');
  //Remove
  fs.rmdir('./new-folder', err => {
    if (err) return console.log('Err when remove folder', err);
    console.log('Successfully remove new-folder');
  });
});