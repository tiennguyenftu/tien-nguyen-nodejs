const fs = require('fs');
const path = require('path');

const oldPath = path.join(__dirname, 'files', 'file-to-rename.txt');
const newPath = path.join(__dirname, 'files', 'file-renamed.txt');

//Synchronous
// //Rename
// fs.renameSync(oldPath, newPath);
// console.log('Successfully rename file-to-rename.txt to file-renamed.txt');
// //Remove
// fs.unlinkSync(newPath);
// console.log('Successfully remove file-renamed.txt');

//Asynchronous
//Rename
fs.rename(oldPath, newPath, err => {
  if (err) return console.log('Error when rename file', err);
  console.log('Successfully rename file-to-rename.txt to file-renamed.txt');
  //Remove
  fs.unlink(newPath, err => {
    if (err) return console.log('Error when remove file', err);
    console.log('Successfully remove file-renamed.txt');
  });
});