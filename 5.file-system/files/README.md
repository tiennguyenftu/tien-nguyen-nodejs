This project includes many sub-projects to demonstrate knowledge about Node.js.  
### `bmi-calculator`:
- Global Objects in Node.js: `process` (`process.stdin`, `process.stdout`, `process.exit`)
- How to run: `node app.js`

### `readline`:
- `readline` node module
- How to run: `node app.js`

### `event-emitter`:
- EventEmitter
- `module.exports`
- `require`
- How to run: `node app.js`

### `child-process`
- `exec`
- `spawn`
- `os` node module
- `setTimeout`, `setInterval` - global timing functions
- How to run: `node exec.js` or `node spawn.js`