const {exec} = require('child_process');
const os = require('os');

// Open a url
const openUrlCmd = os.type().toLowerCase() === 'linux' ? 'sensible-browser' : 'open';
exec(`${openUrlCmd} https://www.google.com.vn`, (err, stdout, stderr) => {
  if (err) return console.log(`exec error: ${err}`);
  console.log(`openUrlCmd - stdout: ${stdout}`);
  if (stderr) {
    console.log(`stderr: ${stderr}`);
  }
});

// Open Terminal
const openTerminalCmd = os.type().toLowerCase() === 'linux' ? 'xterm' : 'open Terminal -a .';
exec(openTerminalCmd, (err, stdout, stderr) => {
  if (err) return console.log(`exec error: ${err}`);
  console.log(`openTerminalCmd - stdout: ${stdout}`);
  if (stderr) {
    console.log(`stderr: ${stderr}`);
  }
});

// Check Node version
exec('node -v', (err, stdout, stderr) => {
  if (err) return console.log(`exec error: ${err}`);
  console.log(`Node version: ${stdout}`);
  if (stderr) {
    console.log(`stderr: ${stderr}`);
  }
});
