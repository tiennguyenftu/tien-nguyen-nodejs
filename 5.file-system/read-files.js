const fs = require('fs');
const path = require('path');

//Read files
//Synchronous
// const files = fs.readdirSync('./files');
// files.map(file => {
//   const filePath = path.join(__dirname, 'files', file);
//   const fileContent = fs.readFileSync(filePath, 'UTF-8');
//   console.log(fileContent);
// });

//Asynchronous
fs.readdir('./files', (err, files) => {
  if (err) return console.log('Error when read files folder', err);
  files.map(file => {
    const filePath = path.join(__dirname, 'files', file);
    fs.readFile(filePath, 'UTF-8', (err, fileContent) => {
      console.log(fileContent);
    });
  });
});