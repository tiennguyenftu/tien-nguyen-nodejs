const WebSocketServer = require("ws").Server;
const wss = new WebSocketServer({ port: 8080 });
console.log('WS server listening on port 8080');

wss.on("connection", function(ws) {
	ws.on("message", (message) => {
		if (message === 'exit') {
			ws.close();
		} else {
			wss.clients.forEach((client) => {
				client.send(message);
			});
		}
	});
	ws.send("Welcome to cyber chat");
});