const quotes = [
  "Have the courage to follow your heart and intuition. They somehow know what you truly want to become.",
  "The people who are crazy enough to think they can change the world are the ones who do.",
  "Stay hungry. Stay foolish.",
  "Your time is limited, so don’t waste it living someone else’s life. Don’t be trapped by dogma—which is living with the results of other people’s thinking. Don’t let the noise of others’ opinions drown out your own inner voice. And most important, have the courage to follow your heart and intuition.",
  "Things don’t have to change the world to be important.",
  "Innovation distinguishes between a leader and a follower.",
  "It’s better to be a pirate than to join the navy."
];

const interval = setInterval(() => {
  const i = Math.floor(Math.random() * quotes.length);
  process.stdout.write(`${quotes[i]} \n`);
}, 1000);

process.stdin.on('data', (data) => {
  console.log(`STDIN Data Recieved -> ${data.toString().trim()}`);
  clearInterval(interval);
  process.exit();
});