const {spawn} = require("child_process");
const cp = spawn("node", ["quotes"]);

cp.stdout.on("data", (data) => {
  console.log(`STDOUT: ${data.toString()}`);
});

cp.on("close", () => {
  console.log("Child Process has ended");
  process.exit();
});

setTimeout(() => {
  cp.stdin.write("stop");
}, 6000);