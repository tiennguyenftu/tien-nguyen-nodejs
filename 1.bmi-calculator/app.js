function calcBMI(w, h) {
  return (w / (h * h)).toFixed(2);
}

function printMessage(result) {
  if (result < 16.0) {
    return 'Very Severely Underweight';
  } else if (result >= 16.0 && result < 17.0) {
    return 'Severely Underweight';
  } else if (result >= 17.0 && result < 18.5) {
    return 'Underweight';
  } else if (result >= 18.5 && result < 25.0) {
    return 'Normal';
  } else if (result >= 25.0 && result < 30.0) {
    return 'Overweight';
  } else if (result >= 30.0 && result < 35.0) {
    return 'Obese Class I';
  } else if (result >= 35.0 && result < 40.0) {
    return 'Obese Class II';
  } else {
    return 'Obese Class III';
  }
}

const questions = [
  'Please enter your weight (kg): ',
  'And your height (m): '
];

const answers = [];

function ask(i) {
  process.stdout.write(`${questions[i]}\n`);
  process.stdout.write("> ");
}

process.stdin.on('data', data => {
  const inputStr = data.toString().trim();
  const inputNum = Number(inputStr);
  answers.push(inputNum);

  if (answers.length < questions.length) {
    ask(answers.length);
  } else {
    process.exit();
  }
});

process.on('exit', () => {
  const [w, h] = answers;
  if (isNaN(w) || isNaN(h)) {
    process.stdout.write('\nInvalid inputs\n');
  } else {
    const result = calcBMI(w, h);
    const message = printMessage(result);
    process.stdout.write(`\nResult: ${result}\n`);
    process.stdout.write(`You are ${message}\n`);
  }
});

process.stdout.write('BMI Calculator\n');
ask(0);